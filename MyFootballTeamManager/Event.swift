//
//  Event.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/1/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

enum TypeEvent {
    case Training
    case Game
}

enum ActionWithEvent {
    case Delete
    case Edit
    case GoToEvent
}


class Event: Object {
    
    @objc dynamic var identity = UUID().uuidString
    @objc dynamic var matchDate = Date()
    @objc dynamic var isTraining = false
    let players = List<Player>()
    @objc dynamic var team: Team?
    @objc dynamic var cost: Double = 75
    let payments = List<Payment>()
    @objc dynamic var paymentMetod: PaymentMetod?
    @objc dynamic var isFormed = false
    
    override static func primaryKey() -> String? {
        return "identity"
    }
}

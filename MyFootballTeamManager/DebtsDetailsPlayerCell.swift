//
//  DetailsDebtsPlayerCell.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/3/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import GMStepper
import BEMCheckBox

class DebtsDetailsPlayerCell: UITableViewCell {
    
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var trainingLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var costPerEvent: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var stepper: GMStepper!
    @IBOutlet weak var checkBox: BEMCheckBox!
    var action: ((Double, Bool) -> ())!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        checkBox.delegate = self
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        teamLabel.text = nil
        trainingLabel.text = nil
        dateLabel.text = nil
        costPerEvent.text = nil
        paidLabel.text = nil
        stepper.value = 0
        checkBox.setOn(false, animated: false)
        stepper.isEnabled = true
    }
    
    func configure(viewModel: DebtDetailsPaymentsForPlayerViewModel) {
        
        teamLabel.text = viewModel.teamName
        trainingLabel.text = viewModel.isTraining
        dateLabel.text = viewModel.dateEvent
        costPerEvent.text = String(viewModel.costPerEvent)
        paidLabel.text = String(viewModel.paid)
        stepper.value = viewModel.valueForStepper
        self.action = viewModel.action
        checkBox.setOn(viewModel.isPaid, animated: false)
    }
}

extension DebtsDetailsPlayerCell: BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        
        stepper.isEnabled = checkBox.on ? false : true
        action(stepper.value, checkBox.on)
    }
}

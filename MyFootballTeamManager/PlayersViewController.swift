//
//  PlayersViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/1/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

private let rowHeight: CGFloat = 50
private let segueEditPlayerProfileVC = "editPlayerProfileVC"
private let segueMakeNewPlayerProfileVC = "makeNewPlayerProfileVC"


class PlayersViewController: UIViewController {
    
    @IBOutlet fileprivate weak var searchTextField: UITextField!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    fileprivate var players: [PlayerCellViewModel] = []
    @IBOutlet weak var helpsButtonAdd30Players: UIBarButtonItem!
    
    private func setup() {
        tableView.register(UINib(nibName: "PlayersCell", bundle: nil), forCellReuseIdentifier: "SearchPlayers")
    }
    
    fileprivate func configure(searchPredicate: String?) {
        
        let predicate = NSPredicate(format: "secondName CONTAINS [nc]%@ OR firstName CONTAINS [nc]%@", searchPredicate ?? "", searchPredicate ?? "")
        players = RealmManager.allPlayers().filter(predicate).map(PlayerCellViewModel.init(player: ))
        
        if players.count >= 30 {
            helpsButtonAdd30Players.isEnabled = false
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure(searchPredicate: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueEditPlayerProfileVC {
            if let viewController = segue.destination as? PlayerProfileVC {
                let indexPath = tableView.indexPathForSelectedRow!
                viewController.editPlayerIdentity = players[indexPath.row].identity
                
                tableView.deselectRow(at: indexPath, animated: false)
            }
        } else if segue.identifier == segueMakeNewPlayerProfileVC {
            
        }
    }
    
    @IBAction private func addNewPlayerAction(_ sender: UIButton) {
        performSegue(withIdentifier: segueMakeNewPlayerProfileVC, sender: self)
    }
    
    @IBAction private func returnPlayerProfile(segue:UIStoryboardSegue) {
        print("return")
        configure(searchPredicate: nil)
    }
    
    @IBAction func helpsButtonAdd30PlayersAction(_ sender: UIBarButtonItem) {
        
        let firstNamesForPlayers = ["Николай", "Егор", "Василий", "Алексей", "Иван", "Павел", "Инокентий", "Сергей", "Александр", "Кирилл", "Юрий", "Максим", "Павел", "Инокентий", "Сергей","Алексей", "Александр", "Алексей", "Иван", "Павел", "Роман", "Сергей", "Александр", "Кирилл", "Иван", "Павел", "Инокентий", "Сергей", "Александр", "Кирилл"]
        let secondNamesForPlayers = ["Еремин", "Иванченко", "Рогов", "Марушко", "Ровский", "Зюзенков", "Костюкевич", "Готовка", "Шаповал", "Громик", "Васечкин", "Горощеня", "Кудин", "Звездунов", "Храмцов","Хрусталев", "Волович", "Алексеев", "Иванеринко", "Короткий", "Еремин", "Хоруженко", "Анципорович", "Кирилленко", "Лагутко", "Шашкин", "Гораютин", "Веселчак", "Баранов", "Атокамонец"]
        
        for index in 0..<30 {
            let player = Player()
            player.firstName = firstNamesForPlayers[index]
            player.secondName = secondNamesForPlayers[index]
            RealmManager.save(object: player)
        }
        setup()
        configure(searchPredicate: nil)
    }
}

extension PlayersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlayers", for: indexPath) as! PlayersCell
        cell.configureForPlayersVC(viewModel: players[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueEditPlayerProfileVC, sender: self)
    }
}

extension PlayersViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        configure(searchPredicate: searchTextField.text! + string)
        return true
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        configure(searchPredicate: nil)
        return true
    }
}

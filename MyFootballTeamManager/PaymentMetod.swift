//
//  PaymentMetod.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/20/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

class PaymentMetod: Object {
    
    @objc dynamic var identity = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var costForPlayerInEvent: Double = 0
    @objc dynamic var costForPlayerOutEvent: Double = 0
    @objc dynamic var divideAtAllTeamPlayers: Bool = false
    @objc dynamic var fixedPayment: Bool = false
    
    override static func primaryKey() -> String? {
        return "identity"
    }
}

//
//  TeamViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 8/25/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

private let rowHeight: CGFloat = 120
private let segueGoToTeamProfile = "goToTeamProfile"
private let segueEditTeamProfile = "editTeamProfile"


class TeamViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    fileprivate var teams: [TeamCellViewModel] = []
    
    private func setup() {
        tableView.register(UINib(nibName: "TeamCell", bundle: nil), forCellReuseIdentifier: "TeamCell")
    }
    
    private func configureTeams () {
        teams = []
        
        for helpTeam in RealmManager.allTeams() {
            teams.append(TeamCellViewModel(team: helpTeam))
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configureTeams()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueEditTeamProfile {
            if let viewController = segue.destination as? TeamProfileVC {
                let indexPath = tableView.indexPathForSelectedRow!
                viewController.editTeamIdentity = teams[indexPath.row].identity
                tableView.deselectRow(at: indexPath, animated: true)
            }
        } else if segue.identifier == segueGoToTeamProfile {
            
        }
    }
    
    @IBAction private func addNewTeamAction(_ sender: UIButton) {
        performSegue(withIdentifier: segueGoToTeamProfile, sender: self)
    }
    
    @IBAction private func cancelTeamProfile(segue:UIStoryboardSegue) {
        configureTeams()
    }
}

extension TeamViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamCell
        cell.configure(model: teams[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueEditTeamProfile, sender: self)
    }
}

//
//  EventViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/4/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

private let rowHeight: CGFloat = 50


class EventViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var formedBarButton: UIBarButtonItem!
    @IBOutlet weak var optionsBarButton: UIBarButtonItem!
    
    fileprivate var event: Event!
    fileprivate var models: [PlayerCellForEventViewModel] = []
    fileprivate var notificationToken: NotificationToken!
    
    var editEventIdentity: String? = nil
    
    var allPaymentsForEvent: Results<Payment>!
    
    
    private func setup() {
        tableView.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "Player")
        if let editEventIdentity = editEventIdentity {
            event = RealmManager.allEvents().filter("identity =  %@", editEventIdentity).first
        } else {
            event = Event()
        }
        configureBarButtons()
        
        RealmManager.checkAndCreatePayments(forEvent: event)
        tableView.backgroundView = UIImageView(image: UIImage(named: "fifa"))
    }
    
    fileprivate func configure() {
        
        let sortProp = [SortDescriptor(keyPath: "playerInEvent", ascending: false), SortDescriptor(keyPath: "playerSecondName", ascending: true)]
        allPaymentsForEvent = RealmManager.allPayments().filter("event.identity == %@", event.identity).sorted(by: sortProp)
        
        for payment in allPaymentsForEvent {
            guard let team = event.team else { break }
            
            if !team.players.contains(payment.player) && (!payment.isPaid || (payment.isPaid && payment.alreadyPaid == 0)  ){
                RealmManager.delete(realmObject: .Payment, identity: payment.identity)
            }
        }
        
        updateModels()
        updateScreen()
        
        notificationToken = allPaymentsForEvent.observe{ [weak self] result in
            switch result {
            case .initial:
                self?.tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                
                self?.updateModels()
                
                self?.tableView.beginUpdates()
                self?.tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }), with: .right)
                self?.tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}), with: .right)
                self?.tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }), with: .fade)
                self?.tableView.endUpdates()
                
                break
            case .error(let error):
                fatalError("\(error)")
                break
            }
        }
    }
    
    private func configureBarButtons() {
        if event.isFormed {
            formedBarButton.title = "Formed On"
            optionsBarButton.isEnabled = false
        } else {
            formedBarButton.title = "Formed Off"
            optionsBarButton.isEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        RealmManager.checkAndCreatePayments(forEvent: event)
        configure()
    }
    
    deinit {
        notificationToken?.invalidate()
        print("deinit EventVC")
    }
    
    private func updateScreen() {
        let paid = allPaymentsForEvent.filter{ $0.isPaid }.reduce(0){ $0 + $1.alreadyPaid }
        paidLabel.text = String(paid)
        costLabel.text = String(event.cost)
    }
    
    fileprivate func updateModels() {
        models.removeAll()
        
        let allPaymentsPlayersInEvent = allPaymentsForEvent.filter{ $0.playerInEvent }
        
        for payment in allPaymentsForEvent {
            models.append(PlayerCellForEventViewModel.init(payment: payment, countPlayersInEvent: allPaymentsPlayersInEvent.count, action: { [weak self] (value, isOn) in
                RealmManager.write {
                    payment.isPaid = isOn
                    payment.alreadyPaid = isOn ? value : 0
                    payment.underOverPaid = payment.alreadyPaid - payment.needToPay
                }
                self?.configure()
            }))
        }
    }
    
    @IBAction private func optionsAction(_ sender: UIBarButtonItem) {
        
        if let editEventIdentity = editEventIdentity {
            let viewController = UIStoryboard(name: "Calendar", bundle: nil).instantiateViewController(withIdentifier: "eventDetails") as! EventDetailsTableViewController
            viewController.eventData.identity = editEventIdentity
            
            for (number, vc) in (navigationController?.viewControllers.enumerated())! {
                if vc == self {
                    viewController.eventData.numberVCForPopTo = number
                    break
                }
            }
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func formedAction(_ sender: UIBarButtonItem) {
        
        RealmManager.write {
            event.isFormed = event.isFormed ? false : true
        }
        setNeedToPayInPayments()
        configureBarButtons()
    }
    
    private func setNeedToPayInPayments() {
        RealmManager.write {
            if event.isFormed {
                for (number, payment) in allPaymentsForEvent.enumerated() {
                    payment.needToPay = models[number].needToPay
                    payment.underOverPaid = payment.alreadyPaid - payment.needToPay
                    payment.eventIsFormed = true
                }
            } else {
                allPaymentsForEvent.setValue(false, forKey: "eventIsFormed")
            }
        }
    }
}

extension EventViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Player", for: indexPath) as! EventCell
        cell.configure(viewModel: models[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard !event.isFormed else {
            tableView.deselectRow(at: indexPath, animated: false)
            return
        }
        
        RealmManager.write {
            if models[indexPath.row].playerInEvent {
                allPaymentsForEvent[indexPath.row].playerInEvent = false
            } else {
                allPaymentsForEvent[indexPath.row].playerInEvent = true
            }
        }
        self.configure()
        if event.paymentMetod?.fixedPayment == false && event.paymentMetod?.divideAtAllTeamPlayers == false {
            tableView.reloadData()
        }
    }
}

//
//  PlayerCellViewModel.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/19/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

struct PlayerCellViewModel {
    
    var firstName: String
    var secondName: String
    var identity: String
    var involvedPlayer: Bool
    
    init(player: Player) {
        firstName = player.firstName
        secondName = player.secondName
        identity = player.identity
        involvedPlayer = player.involvedPlayer
    }
}

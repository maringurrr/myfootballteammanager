//
//  DetailsPaymentsForPlayerViewModel.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/3/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import CVCalendar


struct DebtDetailsPaymentsForPlayerViewModel {
    
    var teamName: String
    var isTraining: String
    var dateEvent: String
    var costPerEvent: Double
    var paid: Double
    var valueForStepper: Double
    var isPaid: Bool
    var action: ((Double, Bool) -> ())
    
    init(payment: Payment, oldValueAlreadyPaid: Double?, action: @escaping (Double, Bool) -> ()) {
        teamName = payment.event.team?.name ?? ""
        isTraining = payment.event.isTraining ? "Training" : "Game"
        dateEvent = CVDate(date: payment.event.matchDate).commonDescription
        costPerEvent = payment.needToPay
        paid = payment.alreadyPaid
        valueForStepper = costPerEvent - paid
        self.action = action
        
        if payment.alreadyPaid == oldValueAlreadyPaid {
            isPaid = false
            valueForStepper = costPerEvent - paid
        } else {
            isPaid = true
            valueForStepper = payment.alreadyPaid - (oldValueAlreadyPaid ?? 0)
        }
    }
}

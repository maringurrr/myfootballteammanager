//
//  DebtPlayerCell.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/2/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import GMStepper

class DebtPlayerCell: UITableViewCell {
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var debtLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        firstNameLabel.text = nil
        secondNameLabel.text = nil
        debtLabel.text = nil
    }
    
    func configure(viewModel: DebtPlayerViewModel) {
        firstNameLabel.text = viewModel.firstName
        secondNameLabel.text = viewModel.secondName
        debtLabel.text = String(viewModel.debt)
    }
}

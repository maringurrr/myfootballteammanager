//
//  Payment.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/16/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

class Payment: Object {
    
    @objc dynamic var identity = UUID().uuidString
    @objc dynamic var playerSecondName = ""
    @objc dynamic var player: Player!
    @objc dynamic var event: Event!
    @objc dynamic var paymentMetod: PaymentMetod?
    @objc dynamic var underOverPaid: Double = 0
    @objc dynamic var alreadyPaid: Double = 0
    @objc dynamic var needToPay: Double = 0
    @objc dynamic var isPaid = false
    @objc dynamic var playerInEvent = true
    @objc dynamic var eventIsFormed = false
    
    
    override static func primaryKey() -> String? {
        return "identity"
    }
}

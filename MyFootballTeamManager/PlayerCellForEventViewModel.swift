//
//  PlayerCellForEventViewModel.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/6/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

struct PlayerCellForEventViewModel {
    
    var firstName: String
    var secondName: String
    var identity: String
    var playerInEvent: Bool
    var action: (Double, Bool) -> ()
    var isPaid: Bool
    var divideAtAllTeamPlayers: Bool
    var fixedPayment: Bool
    var eventCost: Double
    
    var underOverPaid: Double = 0
    var alreadyPaid: Double = 0
    var needToPay: Double = 0
    
    var valueAlpha: CGFloat = 1.0
    var checkBoxIsEnable = false
    var stepperValue: Double
    var stepperMinValue: Double = 0
    var stepperMaxValue: Double = 99
    
    init(payment: Payment, countPlayersInEvent: Int, action: @escaping (Double, Bool) -> ()) {
        firstName = payment.player.firstName
        secondName = payment.player.secondName
        identity = payment.identity
        isPaid = payment.isPaid
        self.action = action
        divideAtAllTeamPlayers = payment.event.paymentMetod?.divideAtAllTeamPlayers ?? false
        fixedPayment = payment.event.paymentMetod?.fixedPayment ?? false
        eventCost = payment.event.cost
        playerInEvent = payment.playerInEvent
        
        
        let countPlayersInTeam = payment.event.team?.players.count ?? 0

        checkBoxIsEnable = payment.isPaid
        
        if payment.playerInEvent == false {
            valueAlpha = 0.5
            if divideAtAllTeamPlayers {
                checkBoxIsEnable = true
            }
        } else {
            valueAlpha = 1
            checkBoxIsEnable = true
        }
        
        if fixedPayment {
            needToPay = playerInEvent ? (payment.event.paymentMetod?.costForPlayerInEvent ?? 0) : (payment.event.paymentMetod?.costForPlayerOutEvent ?? 0)
        } else {
            if divideAtAllTeamPlayers {
                if countPlayersInTeam != 0 {
                    needToPay = (eventCost / Double(countPlayersInTeam) * 2).rounded(.awayFromZero) / 2
                }
            } else {
                if playerInEvent {
                    if countPlayersInEvent != 0 {
                        needToPay = (eventCost / Double(countPlayersInEvent) * 2).rounded(.awayFromZero) / 2
                    }
                }
            }
        }
        stepperValue = payment.isPaid ? payment.alreadyPaid : needToPay
        
        if payment.isPaid {
            stepperMaxValue = stepperValue
            stepperMinValue = stepperValue
        }
    }
}

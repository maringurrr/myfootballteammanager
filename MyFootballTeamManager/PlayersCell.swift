//
//  PlayersCell.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/4/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class PlayersCell: UITableViewCell {
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var marginsView: UIView!
    @IBOutlet weak var inTeamLabel: UILabel!
    
    func configure(viewModel: PlayerCellViewModel) {
        if viewModel.involvedPlayer == false {
            marginsView.alpha = 0.6
            inTeamLabel.text = ""
        } else {
            marginsView.alpha = 1
            inTeamLabel.text = "In Team"
        }
        firstNameLabel.text = viewModel.firstName
        secondNameLabel.text = viewModel.secondName
    }
    
    func configureForPlayersVC(viewModel: PlayerCellViewModel) {
        firstNameLabel.text = viewModel.firstName
        secondNameLabel.text = viewModel.secondName
        inTeamLabel.text = ""
    }
}

//
//  Team.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/5/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

class Team: Object {
    @objc dynamic var name = ""
    @objc dynamic var identity = UUID().uuidString
    let players = List<Player>()
    let event = List<Event>()
    
    override static func primaryKey() -> String? {
        return "identity"
    }
}

//
//  TeamCell.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/5/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {

    @IBOutlet weak var imageTeam: UIImageView!
    @IBOutlet weak var nameTeamLabel: UILabel!
    @IBOutlet weak var numberOfPlayersInTeamLabel: UILabel!
    
    func configure(model: TeamCellViewModel) {
        nameTeamLabel.text = model.name
        numberOfPlayersInTeamLabel.text = String(model.players.count)
    }
}

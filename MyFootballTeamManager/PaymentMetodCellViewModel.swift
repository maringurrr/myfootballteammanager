//
//  PaymentMetodCellViewModel.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/20/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

struct PaymentMetodCellViewModel {
    
    var identity: String
    var name: String
    var costForPlayerInEvent: Double
    var costForPlayerOutEvent: Double
    var divideAtAllTeamPlayers: Bool
    var fixedPayment: Bool
    
    init(paymentMetod: PaymentMetod) {
        identity = paymentMetod.identity
        name = paymentMetod.name
        costForPlayerInEvent = paymentMetod.costForPlayerInEvent
        costForPlayerOutEvent = paymentMetod.costForPlayerOutEvent
        divideAtAllTeamPlayers = paymentMetod.divideAtAllTeamPlayers
        fixedPayment = paymentMetod.fixedPayment
    }
}

//
//  PaymentMetodProfileVC.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/20/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import GMStepper
import BEMCheckBox

private let segueExitPaymentMetodProfile = "exitPaymentMetodProfile"

class PaymentMetodProfileVC: UIViewController {
    
    @IBOutlet private  weak var deleteBarButton: UIBarButtonItem!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet fileprivate weak var wasAtTheGameStepper: GMStepper!
    @IBOutlet fileprivate weak var wasNotAtTheGameStepper: GMStepper!
    @IBOutlet fileprivate weak var payAllTeamPlayersCheckBox: BEMCheckBox!
    @IBOutlet fileprivate weak var fixedPaymentCheckBox: BEMCheckBox!
    
    var editPaymentMetodIdentity: String? = nil
    
    private var paymentMetod: PaymentMetod!
    
    
    private func setup() {
        
        payAllTeamPlayersCheckBox.delegate = self
        fixedPaymentCheckBox.delegate = self
        
        if let editPaymentMetodIdentity = editPaymentMetodIdentity {
            paymentMetod = RealmManager.allPaymentMetods().filter("identity =  %@", editPaymentMetodIdentity).first
        }else {
            paymentMetod = PaymentMetod()
            deleteBarButton.isEnabled = false
            nameTextField.becomeFirstResponder()
        }
    }
    
    fileprivate func configure() {
        
        nameTextField.text = paymentMetod.name
        payAllTeamPlayersCheckBox.on = paymentMetod.divideAtAllTeamPlayers
        fixedPaymentCheckBox.on = paymentMetod.fixedPayment
        wasAtTheGameStepper.value = paymentMetod.fixedPayment ? paymentMetod.costForPlayerInEvent : 0
        wasNotAtTheGameStepper.value = paymentMetod.fixedPayment ? paymentMetod.costForPlayerOutEvent : 0
        if !paymentMetod.fixedPayment {
            wasAtTheGameStepper.isEnabled = false
            wasNotAtTheGameStepper.isEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure()
    }
    
    deinit {
        print("deinit PaymentMetodVC")
    }
    
    private func savePaymentMetod() {
        let paymentMetodForSave = PaymentMetod()
        paymentMetodForSave.name = self.nameTextField.text ?? ""
        paymentMetodForSave.divideAtAllTeamPlayers = payAllTeamPlayersCheckBox.on
        paymentMetodForSave.fixedPayment = fixedPaymentCheckBox.on
        paymentMetodForSave.costForPlayerInEvent = wasAtTheGameStepper.value
        paymentMetodForSave.costForPlayerOutEvent = wasNotAtTheGameStepper.value
        
        if let editPaymentMetodIdentity = editPaymentMetodIdentity {
            paymentMetodForSave.identity = editPaymentMetodIdentity
        }
        RealmManager.save(object: paymentMetodForSave)
    }
    
    private func deletePaymentMetod() {
        if let editPlayerIdentity = editPaymentMetodIdentity {
            RealmManager.delete(realmObject: .PaymentMetod, identity: editPlayerIdentity)
        }
    }
    
    @IBAction private func deleteBarButtonAction(_ sender: UIBarButtonItem) {
        
        AlertMessage.simpleWithActionOkAlertMessage(titleMessage: "Do you want to delete this paymentMetod?", alertMsg: nil) {
            [weak self] in
            self?.deletePaymentMetod()
            self?.performSegue(withIdentifier: segueExitPaymentMetodProfile, sender: self)
        }
    }
    
    @IBAction private func savePaymentMetodAction(_ sender: UIBarButtonItem) {
        
        if nameTextField.text != "" {
            let title = editPaymentMetodIdentity == nil ? "Do you want to add this paymentMetod?" : "Do you want to edit this paymentMetod?"
            
            AlertMessage.simpleWithActionOkAlertMessage(titleMessage: title, alertMsg: "\(nameTextField.text ?? "")", {
                [weak self] in
                
                self?.savePaymentMetod()
                self?.performSegue(withIdentifier: segueExitPaymentMetodProfile, sender: self)
            })
        } else {
            AlertMessage.simpleAlertMessage(titleMessage: "Please, enter paymentMetod`s name", alertMsg: nil)
        }
    }
}

extension PaymentMetodProfileVC: BEMCheckBoxDelegate {
    
    func didTap(_ checkBox: BEMCheckBox) {
        if checkBox == fixedPaymentCheckBox {
            if checkBox.on {
                wasAtTheGameStepper.isEnabled = true
                wasNotAtTheGameStepper.isEnabled = true
            } else {
                wasAtTheGameStepper.value = 0
                wasNotAtTheGameStepper.value = 0
                wasAtTheGameStepper.isEnabled = false
                wasNotAtTheGameStepper.isEnabled = false
            }
        }
    }
}

//
//  Player.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 8/25/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

class Player: Object {
    @objc dynamic var firstName = ""
    @objc dynamic var secondName = ""
    @objc dynamic var identity = UUID().uuidString
    @objc dynamic var involvedPlayer = false
    
    let team = LinkingObjects(fromType: Team.self, property: "players")
    let event = LinkingObjects(fromType: Event.self, property: "players")
    let payment = LinkingObjects(fromType: Payment.self, property: "player")
    
    override static func primaryKey() -> String? {
        return "identity"
    }
}


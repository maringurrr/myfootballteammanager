//
//  PlayerProfileVC.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/1/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

private let segueExitPlayerProfile = "exitPlayerProfile"


class PlayerProfileVC: UIViewController {
    
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var secondNameTextField: UITextField!
    @IBOutlet private weak var dateOfBirthTextField: UITextField!
    @IBOutlet private weak var yearInTeamTextField: UITextField!
    @IBOutlet private weak var deleteBarButton: UIBarButtonItem!
    
    var editPlayerIdentity: String? = nil
    
    private func setup() {
        if let editPlayerIdentity = editPlayerIdentity {
            let player = RealmManager.allPlayers().filter("identity =  %@", editPlayerIdentity).first
            firstNameTextField.text = player?.firstName ?? ""
            secondNameTextField.text = player?.secondName ?? ""
        }else {
            deleteBarButton.isEnabled = false
            firstNameTextField.becomeFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    deinit {
        print("deinit PlayerProfileVC")
    }
    
    private func savePlayer() {
        let playerForSave = Player()
        playerForSave.firstName = self.firstNameTextField.text!.capitalized
        playerForSave.secondName = self.secondNameTextField.text!.capitalized
        if let editPlayerIdentity = editPlayerIdentity {
            playerForSave.identity = editPlayerIdentity
        }
        RealmManager.save(object: playerForSave)
    }
    
    private func deletePlayer() {
        if let editPlayerIdentity = editPlayerIdentity {
            RealmManager.delete(realmObject: .Player, identity: editPlayerIdentity)
        }
    }
    
    @IBAction private func deleteBarButtonAction(_ sender: UIBarButtonItem) {
        
        AlertMessage.simpleWithActionOkAlertMessage(titleMessage: "Do you want to delete this player?", alertMsg: nil) {
            [weak self] in
            self?.deletePlayer()
            self?.performSegue(withIdentifier: segueExitPlayerProfile, sender: self)
        }
    }
    
    @IBAction private func savePlayerAction(_ sender: UIBarButtonItem) {
        var message = ""
        if firstNameTextField.text != "" && secondNameTextField.text != "" {
            
            let title = editPlayerIdentity == nil ? "Do you want to add this player?" : "Do you want to edit this player?"
            
            AlertMessage.simpleWithActionOkAlertMessage(titleMessage: title, alertMsg: "\(firstNameTextField.text ?? "") \(secondNameTextField.text ?? "")", {
                [weak self] in
                
                self?.savePlayer()
                self?.performSegue(withIdentifier: segueExitPlayerProfile, sender: self)
            })
            
        } else {
            if firstNameTextField.text == "" && secondNameTextField.text == "" {
                message = "Please, enter first name and second name"
            } else if firstNameTextField.text == "" {
                message = "Please, enter first name"
            } else {
                message = "Please, enter second name"
            }
            AlertMessage.simpleAlertMessage(titleMessage: message, alertMsg: nil)
        }
    }
}

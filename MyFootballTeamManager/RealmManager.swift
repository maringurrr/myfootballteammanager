//
//  RealmManager.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/7/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation
import RealmSwift

enum RealmObject {
    case Player
    case Team
    case Event
    case Payment
    case PaymentMetod
}

class RealmManager {
    
    static let playerSortedByKeyPath = "secondName"
    static let teamSortedByKeyPath = "name"
    static let eventSortedByKeyPath = "matchDate"
    
    
    static var realm: Realm {
        var realm: Realm! = nil
        realm = try! Realm()
        return realm
    }
    
    static func allPlayers() -> Results<Player> {
        return realm.objects(Player.self).sorted(byKeyPath: playerSortedByKeyPath)
    }
    
    //    static func allPlayersSortByInvolved() -> Results<Player> {
    //        let sortProperties = [SortDescriptor(keyPath: "event", ascending: true), SortDescriptor(keyPath: "secondName", ascending: true)]
    //        return realm.objects(Player.self).sorted(by: sortProperties)
    //    }
    
    static func allTeams() -> Results<Team> {
        return realm.objects(Team.self).sorted(byKeyPath: teamSortedByKeyPath)
    }
    
    static func allEvents() -> Results<Event> {
        return realm.objects(Event.self).sorted(byKeyPath: eventSortedByKeyPath)
    }
    
    static func allPayments() -> Results<Payment> {
        return realm.objects(Payment.self)
    }
    
    static func allPaymentMetods() -> Results<PaymentMetod> {
        return realm.objects(PaymentMetod.self)
    }
    
    
    static func getPlayersWith(secondName: String) -> [Player] {
        return realm.objects(Player.self).filter("secondName CONTAINS [nc]%@ OR firstName CONTAINS [nc]%@", secondName, secondName).sorted(byKeyPath: playerSortedByKeyPath).map{$0}
    }
    
    static func getTeam(forName: String) -> Team? {
        return realm.objects(Team.self).filter("name =  %@", forName).first
    }
    
    static func getPaymentMetod(forName: String) -> PaymentMetod? {
        return realm.objects(PaymentMetod.self).filter("name =  %@", forName).first
    }
    
    static func getEvent(forDate date: Date) -> Event? {
        return realm.objects(Event.self).filter("matchDate =  %@", date).first
    }
    
    static func getPayment(event: Event, player: Player) -> Payment? {
        
        return realm.objects(Payment.self).filter("player.identity = %@ AND event.identity = %@", player.identity, event.identity).first
    }
    
    static func getPlayer(identity: String) -> Player? {
        return realm.objects(Player.self).filter("identity = %@", identity).first
    }
    
    
    static func deleteAllPaymentsForEvent(event: Event) {
        for payment in allPayments().filter("event.identity = %@", event.identity) {
            delete(realmObject: .Payment, identity: payment.identity)
        }
    }
    
    static func checkAndCreatePayments(forEvent event: Event) {
        guard let team = event.team else { return }
        for player in team.players {
            if getPayment(event: event, player: player) == nil {
                let payment = Payment()
                payment.event = event
                payment.player = player
                payment.playerSecondName = player.secondName
                payment.paymentMetod = payment.event.paymentMetod
                save(object: payment)
            }
        }
    }
    
    //    static func getPayment(forDate date: Date) -> Payment? {
    //        return realm.objects(Payment.self).filter("player =  %@", date).first
    //    }
    
    static func save(object: Object) {
        write {
            realm.add(object, update: true)
        }
    }
    
    static func delete(realmObject: RealmObject, identity: String) {
        write {
            switch realmObject {
            case .Player:
                if let playerForDelete = realm.objects(Player.self).filter("identity =  %@", identity).first {
                    realm.delete(playerForDelete)
                }
            case .Team:
                if let teamForDelete = realm.objects(Team.self).filter("identity =  %@", identity).first {
                    realm.delete(teamForDelete)
                }
            case .Event:
                if let eventForDelete = realm.objects(Event.self).filter("identity =  %@", identity).first {
                    realm.delete(eventForDelete)
                }
            case .Payment:
                if let paymentForDelete = realm.objects(Payment.self).filter("identity =  %@", identity).first {
                    realm.delete(paymentForDelete)
                }
            case .PaymentMetod:
                if let paymentMetodForDelete = realm.objects(PaymentMetod.self).filter("identity =  %@", identity).first {
                    realm.delete(paymentMetodForDelete)
                }
            }
        }
    }
    
    static func write(_ closure: () -> ()) {
        try! realm.write(closure)
    }
}

//
//  ViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 8/25/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import CVCalendar

let segueGoToEventSegue = "goToEvent"
let segueGoToCalendarSegue = "goToCalendarStoryboard"


class MainViewController: UIViewController {
    
    @IBOutlet private weak var midLabel: UIButton!
    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var botLabel: UILabel!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var previousButton: UIButton!
    
    private var events = [Event] ()
    private var startIndex = 0
    private var isFutureEvent: Bool = false
    
    
    private func configureEvents() {
        events = Array(RealmManager.allEvents().sorted(byKeyPath: "matchDate", ascending: true))
    }
    
    private func getDate() {
        let todeyDate = CVDate(date: Date()).convertedDate()!
        for (number, event) in events.enumerated() {
            if event.matchDate >= todeyDate {
                startIndex = number
                isFutureEvent = true
                return
            }
        }
        startIndex = events.count
        isFutureEvent = false
    }
    
    private func setup() {
        print("all Payments count =", RealmManager.allPayments().count)
    }
    
    private func updateUI() {
        if isFutureEvent {
            botLabel.text = CVDate(date: events[startIndex].matchDate).commonDescription
            topLabel.text = events[startIndex].team?.name
            let nameButton = events[startIndex].isTraining ? "Training" : "Game"
            midLabel.setTitle(nameButton, for: .normal)
        } else {
            botLabel.text = ""
            topLabel.text = ""
            midLabel.setTitle("Add new Event", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createАccessory()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        if events == Array(RealmManager.allEvents().sorted(byKeyPath: "matchDate", ascending: true)) {
            configureEvents()
        } else {
            configureEvents()
            getDate()
        }
        updateUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueGoToEventSegue {
            if let viewController = segue.destination as? EventViewController {
                
                viewController.editEventIdentity = events[startIndex].identity
            }
        }
    }
    
    private func createАccessory() {
        
        if RealmManager.getPaymentMetod(forName: "Default") == nil {
            createDefaultPayment()
        }
    }
    
    private func createDefaultPayment() {
        let paymentMetod = PaymentMetod()
        paymentMetod.name = "Default"
        RealmManager.save(object: paymentMetod)
    }
    
    @IBAction private func nextButtonAction(_ sender: UIButton) {
        if startIndex < (events.count - 1) {
            startIndex += 1
        } else if startIndex == (events.count - 1) {
            startIndex += 1
            isFutureEvent = false
        }
        updateUI()
    }
    
    @IBAction private func previousButtonAction(_ sender: UIButton) {
        if startIndex > 0 {
            startIndex -= 1
            isFutureEvent = true
            updateUI()
        }
    }
    
    @IBAction private func midButtonAction(_ sender: UIButton) {
        if startIndex == events.count {
            performSegue(withIdentifier: segueGoToCalendarSegue, sender: nil)
        } else {
            performSegue(withIdentifier: segueGoToEventSegue, sender: nil)
        }
    }
}

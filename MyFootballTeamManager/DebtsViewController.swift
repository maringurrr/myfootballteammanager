//
//  DebtsViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/2/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

private let rowHeight: CGFloat = 50
private let segueGoToDetailsDebts = "goToDetailsDebts"


class DebtsViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var balansValueLabel: UILabel!
    
    fileprivate var models: [DebtPlayerViewModel] = []
    private var allPaymentsWithDebt: Results<Payment>!
    
    
    private func setup() {
        tableView.backgroundView = UIImageView(image: UIImage(named: "fifa"))
        tableView.register(UINib(nibName: "DebtPlayerCell", bundle: nil), forCellReuseIdentifier: "DebtPlayerCell")
    }
    
    private func updateModels() {
        let uniquePlayerIdentitys = Array(Set(allPaymentsWithDebt.map { $0.player.identity }))
        models = uniquePlayerIdentitys.map { DebtPlayerViewModel.init(payments: Array(allPaymentsWithDebt.filter("player.identity = %@", $0))) }
    }
    
    fileprivate func configure() {
        
        allPaymentsWithDebt = RealmManager.allPayments().filter("eventIsFormed = %@ AND underOverPaid != %@", true, 0)
        
        let balansValue: Double = allPaymentsWithDebt.sum(ofProperty: "underOverPaid")
        balansValueLabel.text = String(balansValue)
        
        updateModels()
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        configure()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueGoToDetailsDebts {
            if let viewController = segue.destination as? DebtsDetailsViewController {
                let indexPath = tableView.indexPathForSelectedRow!
                viewController.identityPlayer = models[indexPath.row].identity
                
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
}

extension DebtsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DebtPlayerCell", for: indexPath) as! DebtPlayerCell
        cell.configure(viewModel: models[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueGoToDetailsDebts, sender: self)
    }
}

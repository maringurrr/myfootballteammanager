//
//  DebtsDetailsViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/3/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

private let rowHeight: CGFloat = 70


class DebtsDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var amountDebtsLabel: UILabel!
    
    fileprivate var models: [DebtDetailsPaymentsForPlayerViewModel] = []
    
    private var allPaymentsForPLayer = [Payment]()
    private var workingPaymentsIdentity = [String]()
    private var oldAlreadyPaids = [String: Double]()
    
    var identityPlayer: String!
    
    
    private func setup() {
        tableView.backgroundView = UIImageView(image: UIImage(named: "fifa"))
        tableView.register(UINib(nibName: "DebtsDetailsPlayerCell", bundle: nil), forCellReuseIdentifier: "DebtsDetailsPlayerCell")
        
        allPaymentsForPLayer = Array(RealmManager.allPayments().filter("player.identity = %@ AND eventIsFormed = %@ AND underOverPaid != %@", identityPlayer, true, 0))
        
        for payment in allPaymentsForPLayer {
            workingPaymentsIdentity.append(payment.identity)
            oldAlreadyPaids[payment.identity] = payment.alreadyPaid
        }
        let player = RealmManager.getPlayer(identity: identityPlayer)
        firstNameLabel.text = player?.firstName
        secondNameLabel.text = player?.secondName
    }
    
    fileprivate func configure() {
        updateModels()
        updateAmountDebtsLabel()
        tableView.reloadData()
    }
    
    private func updateModels() {
        
        models.removeAll()
        
        
        for identity in workingPaymentsIdentity {
            let payment = RealmManager.allPayments().filter("identity = %@", identity).first!
            
            models.append(DebtDetailsPaymentsForPlayerViewModel.init(payment: payment, oldValueAlreadyPaid: oldAlreadyPaids[payment.identity], action: { [weak self] (value, isOn) in
                let newAlreadyPaid = payment.alreadyPaid + value
                RealmManager.write {
                    payment.isPaid = isOn
                    payment.alreadyPaid = isOn ? newAlreadyPaid : (self?.oldAlreadyPaids[payment.identity] ?? 0)
                    payment.underOverPaid = payment.alreadyPaid - payment.needToPay
                }
                self?.configure()
            }))
        }
    }
    
    private func updateAmountDebtsLabel() {
        amountDebtsLabel.text = String(models.reduce(0) { $0 + ($1.paid - $1.costPerEvent) })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure()
    }
}

extension DebtsDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DebtsDetailsPlayerCell", for: indexPath) as! DebtsDetailsPlayerCell
        cell.configure(viewModel: models[indexPath.row])
        
        return cell
    }
}

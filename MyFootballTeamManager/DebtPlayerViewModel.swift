//
//  PlayerWithDebtsViewModel.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/2/17.
//  Copyright © 2017 mac-001. All rights reserved.
//


import UIKit

struct DebtPlayerViewModel {
    
    var firstName: String
    var secondName: String
    var identity: String
    var debt: Double
    
    init(payments: [Payment]) {
        firstName = payments[0].player.firstName
        secondName = payments[0].player.secondName
        identity = payments[0].player.identity
        debt = payments.reduce(0) { $0 + $1.underOverPaid }
    }
}

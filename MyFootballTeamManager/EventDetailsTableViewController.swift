//
//  EventDetailsTableViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

private let defaultTrainingCost: Double = 60
private let defaultGameCost: Double = 75
private let segueExitEventProfile = "exitEventProfile"


class EventDetailsTableViewController: UITableViewController {
    
    @IBOutlet private weak var teamNameLabel: UILabel!
    @IBOutlet private weak var trainingLabel: UILabel!
    @IBOutlet private weak var paymentMetodLabel: UILabel!
    @IBOutlet private weak var costLabel: UITextField!
    
    var eventData: (identity: String?, date: Date?, isTraining: Bool?, numberVCForPopTo: Int?)
    var event = Event()
    fileprivate var workingEvent = Event()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        updateUI()
    }
    
    private func setup() {
        
        if let editEventIdentity = eventData.identity {
            event = RealmManager.allEvents().filter("identity =  %@", editEventIdentity).first!
            
            workingEvent.team = event.team
            workingEvent.isTraining = event.isTraining
            workingEvent.paymentMetod = event.paymentMetod
            workingEvent.cost = event.cost
        } else {
            workingEvent.isTraining = eventData.isTraining ?? false
            workingEvent.matchDate = eventData.date ?? Date()
            workingEvent.cost = workingEvent.isTraining ? defaultTrainingCost : defaultGameCost
        }
    }
    
    fileprivate func updateUI() {
        
        teamNameLabel.text = workingEvent.team?.name
        trainingLabel.text = workingEvent.isTraining ? "Yes" : "No"
        paymentMetodLabel.text = workingEvent.paymentMetod?.name
        costLabel.text = String(workingEvent.cost)
        
        tableView.reloadData()
    }
    
    deinit {
        print("deinit EventDetails")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        costLabel.resignFirstResponder()
        switch indexPath.row {
        case 0:
            var arrayNames = [String] ()
            for team in RealmManager.allTeams() {
                arrayNames.append(team.name)
            }
            if arrayNames.count != 0 {
                AlertMessage.changeName(titleMessage: "Change team", alertMsg: nil, arrayWithNames: arrayNames, { [weak self] (teamName) in
                    
                    if let team = RealmManager.getTeam(forName: teamName) {
                        self?.workingEvent.team = team
                        self?.updateUI()
                    }
                })
            } else {
                AlertMessage.simpleAlertMessage(titleMessage: "The list of teams is empty", alertMsg: nil)
            }
        case 1:
            RealmManager.write {
                if workingEvent.isTraining {
                    workingEvent.isTraining = false
                    if workingEvent.cost == defaultTrainingCost {
                        workingEvent.cost = defaultGameCost
                    }
                    
                } else {
                    workingEvent.isTraining = true
                    if workingEvent.cost == defaultGameCost {
                        workingEvent.cost = defaultTrainingCost
                    }
                }
            }
            updateUI()
        case 2:
            var arrayNames = [String] ()
            for paymentMetod in RealmManager.allPaymentMetods() {
                arrayNames.append(paymentMetod.name)
            }
            if arrayNames.count != 0 {
                AlertMessage.changeName(titleMessage: "Change payment metod", alertMsg: nil, arrayWithNames: arrayNames, { [weak self] (name) in
                    if let paymentMetod = RealmManager.getPaymentMetod(forName: name) {
                        self?.workingEvent.paymentMetod = paymentMetod
                        self?.updateUI()
                    }
                })
            } else {
                AlertMessage.simpleAlertMessage(titleMessage: "The list of paymentMetods is empty", alertMsg: nil)
            }
            
        case 3:
            costLabel.becomeFirstResponder()
        default:
            print("default")
        }
    }
    
    private func deleteEventAndPaymentsForEvent() {
        guard let editEventIdentity = eventData.identity else { return }
        
        RealmManager.deleteAllPaymentsForEvent(event: event)
        RealmManager.delete(realmObject: .Event, identity: editEventIdentity)
    }
    
    private func saveEvent() {
        if eventData.identity != nil {
            
            if event.team != workingEvent.team {
                let oldPayments = RealmManager.allPayments().filter("event.identity == %@", event.identity)
                oldPayments.filter { $0.alreadyPaid != 0 && !$0.isPaid }.forEach { RealmManager.delete(realmObject: .Payment, identity: $0.identity) }
            }
            
            RealmManager.write {
                event.team = workingEvent.team
                event.isTraining = workingEvent.isTraining
                event.paymentMetod = workingEvent.paymentMetod
                event.cost = workingEvent.cost
                for player in workingEvent.players {
                    event.players.append(player)
                }
            }
        } else {
            RealmManager.save(object: workingEvent)
        }
    }
    
    @IBAction func saveBarButtonAction(_ sender: UIBarButtonItem) {
        costLabel.resignFirstResponder()
        
        let title = eventData.identity == nil ? "Do you want to add this event?" : "Do you want to edit this event?"
        
        AlertMessage.simpleWithActionOkAlertMessage(titleMessage: title, alertMsg: nil, {
            [weak self] in
            
            self?.saveEvent()
            self?.navigationController?.popViewController(animated: true)
        })
    }
    
    @IBAction func deleteBarButtonAction(_ sender: UIBarButtonItem) {
        
        if event.isFormed {
            AlertMessage.simpleAlertMessage(titleMessage: "Warning", alertMsg: "You cann`t delete this event because it is formed")
        } else {
            AlertMessage.simpleWithActionOkAlertMessage(titleMessage: "Do you want to delete this event and all event`s payments?", alertMsg: nil) {
                [weak self] in
                
                self?.deleteEventAndPaymentsForEvent()
                
                if let number = self?.eventData.numberVCForPopTo {
                    self?.navigationController?.popToViewController((self?.navigationController?.viewControllers[number - 1])!, animated: true)
                } else {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension EventDetailsTableViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let valueDouble = Double(textField.text!) {
            workingEvent.cost = valueDouble
        }
        updateUI()
    }
}


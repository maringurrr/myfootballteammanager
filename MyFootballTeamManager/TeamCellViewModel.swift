//
//  TeamCellViewModel.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/18/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

struct TeamCellViewModel {
    
    var name: String
    var identity: String
    var players: [Player] = []
    
    init(team: Team) {
        name = team.name
        identity = team.identity
        players = Array(team.players)
    }
}

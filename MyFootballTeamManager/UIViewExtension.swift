//
//  UIViewExtension.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 11/3/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit


extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}

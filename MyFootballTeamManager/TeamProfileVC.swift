//
//  TeamProfileVC.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/5/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import RealmSwift

private let rowHeight: CGFloat = 50
private let segueExitTeamProfile = "exitTeamProfile"


class TeamProfileVC: UIViewController {
    
    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet weak fileprivate var nameTeamTextField: UITextField!
    @IBOutlet weak fileprivate var numberOfPlayersInTeamLabel: UILabel!
    @IBOutlet weak private var deleteBarButton: UIBarButtonItem!
    @IBOutlet weak private var imageTeam: UIImageView!
    
    fileprivate var allPlayersResults: Results<Player>!
    private var notificationToken: NotificationToken!
    fileprivate var models: [PlayerCellViewModel] = []
    private var team: Team!
    
    var editTeamIdentity: String? = nil
    
    private func configure() {
        let sortProperties = [SortDescriptor(keyPath: "involvedPlayer", ascending: false), SortDescriptor(keyPath: "secondName", ascending: true)]
        
        allPlayersResults = RealmManager.allPlayers().sorted(by: sortProperties)
        
        updateModels()
        
        notificationToken = allPlayersResults.observe{ [weak self] result in
            switch result {
            case .initial:
                self?.tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                
                self?.updateModels()
                
                self?.tableView.beginUpdates()
                self?.tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }), with: .right)
                self?.tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}), with: .right)
                self?.tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }), with: .fade)
                self?.tableView.endUpdates()
                break
            case .error(let error):
                fatalError("\(error)")
                break
            }
        }
    }
    
    private func setup() {
        
        tableView.register(UINib(nibName: "PlayersCell", bundle: nil), forCellReuseIdentifier: "SearchPlayersInTeam")
        
        if editTeamIdentity == nil {
            deleteBarButton.isEnabled = false
            nameTeamTextField.becomeFirstResponder()
        }
        
        if let editTeamIdentity = editTeamIdentity {
            team = RealmManager.allTeams().filter("identity = %@", editTeamIdentity).first
            nameTeamTextField.text = team.name
            numberOfPlayersInTeamLabel.text = String(team.players.count)
        } else {
            team = Team()
            numberOfPlayersInTeamLabel.text = "0"
        }
        
        let allPlayersOutTeamResults = RealmManager.allPlayers().filter("ANY team.identity == %@", team.identity)
        
        RealmManager.write {
            RealmManager.allPlayers().setValue(false, forKey: "involvedPlayer")
            allPlayersOutTeamResults.setValue(true, forKey: "involvedPlayer")
        }
        tableView.backgroundView = UIImageView(image: UIImage(named: "fifa"))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure()
    }
    
    deinit {
        notificationToken?.invalidate()
        print("deinit TeamProfileVC")
    }
    
    private func deleteTeam() {
        guard let editTeamIdentity = editTeamIdentity else { return }
        RealmManager.delete(realmObject: .Team, identity: editTeamIdentity)
    }
    
    private func saveTeam() {
        let teamForSave = Team()
        teamForSave.name = self.nameTeamTextField.text ?? ""
        
        for player in models {
            if player.involvedPlayer {
                teamForSave.players.append(RealmManager.allPlayers().filter("identity =  %@", player.identity).first!)
            }
        }
        if let editTeamIdentity = editTeamIdentity {
            teamForSave.identity = editTeamIdentity
        }
        RealmManager.save(object: teamForSave)
    }
    
    private func updateModels() {
        models.removeAll()
        var numberPlayers = 0
        for player in allPlayersResults {
            if player.involvedPlayer {
                numberPlayers += 1
            }
            models.append(PlayerCellViewModel.init(player: player))
        }
        numberOfPlayersInTeamLabel.text = String(numberPlayers)
    }
    
    @IBAction private func deleteTeamAction(_ sender: UIBarButtonItem) {
        
        AlertMessage.simpleWithActionOkAlertMessage(titleMessage: "Delete this team?", alertMsg: nil) {
            [weak self] in
            self?.deleteTeam()
            self?.performSegue(withIdentifier: segueExitTeamProfile, sender: self)
        }
    }
    
    @IBAction private func saveTeamAction(_ sender: UIBarButtonItem) {
        
        tableView.endEditing(true)
        
        if nameTeamTextField.text != "" {
            AlertMessage.simpleWithActionOkAlertMessage(titleMessage: "Save this team?", alertMsg: "\(nameTeamTextField.text ?? "")", {
                [weak self] in
                self?.saveTeam()
                self?.performSegue(withIdentifier: segueExitTeamProfile, sender: self)
            })
        } else {
            AlertMessage.simpleAlertMessage(titleMessage: "Please enter name", alertMsg: nil)
        }
    }
}

extension TeamProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlayersInTeam", for: indexPath) as! PlayersCell
        cell.configure(viewModel: models[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        RealmManager.write {
            if models[indexPath.row].involvedPlayer {
                allPlayersResults[indexPath.row].involvedPlayer = false
            } else {
                allPlayersResults[indexPath.row].involvedPlayer = true
            }
        }
        nameTeamTextField.resignFirstResponder()
    }
}


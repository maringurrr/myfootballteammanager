//
//  EventCell.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/6/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import GMStepper
import BEMCheckBox

class EventCell: UITableViewCell {
    
    @IBOutlet weak var marginsView: UIView!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var stepper: GMStepper!
    @IBOutlet weak var checkBox: BEMCheckBox!
    
    var action: ((Double, Bool) -> ())!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        checkBox.delegate = self
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        firstNameLabel.text = nil
        secondNameLabel.text = nil
        stepper.value = 0
        stepper.maximumValue = 99
        stepper.minimumValue = 0
        checkBox.setOn(false, animated: false)
        marginsView.alpha = 1
    }
    
    func configure(viewModel: PlayerCellForEventViewModel) {
        
        firstNameLabel.text = viewModel.firstName
        secondNameLabel.text = viewModel.secondName
        self.action = viewModel.action
        checkBox.setOn(viewModel.isPaid, animated: false)
        stepper.value = viewModel.stepperValue
        marginsView.alpha = viewModel.valueAlpha
        stepper.minimumValue = viewModel.stepperMinValue
        stepper.maximumValue = viewModel.stepperMaxValue
    }
}

extension EventCell: BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        
        action(stepper.value, checkBox.on)
    }
}

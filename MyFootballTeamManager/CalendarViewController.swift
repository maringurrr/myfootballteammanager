//
//  CalendarViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/25/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import CVCalendar

private let segueGoToEventDetails = "GoToEventDetails"
//private let segueMakeNewPlayerProfileVC = "makeNewPlayerProfileVC"


class CalendarViewController: UIViewController {
    
    @IBOutlet private weak var menuView: CVCalendarMenuView!
    @IBOutlet fileprivate weak var calendarView: CVCalendarView!
    @IBOutlet fileprivate weak var monthLabelItem: UINavigationItem!
    
    var eventData: (identity: String?, date: Date?, isTraining: Bool?, numberVCForPopTo: Int?)
    
    fileprivate var allEvents: [Event] = []
    
    private func setup() {
        let date = CVDate(date: Date())
        monthLabelItem.title = date.globalDescription
    }
    
    fileprivate func configure() {
        eventData.identity = nil
        eventData.date = nil
        eventData.isTraining = nil
        eventData.numberVCForPopTo = nil
        allEvents = Array(RealmManager.allEvents())
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configure()
        calendarView.contentController.refreshPresentedMonth()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueGoToEventDetails {
            if let viewController = segue.destination as? EventDetailsTableViewController {
                viewController.eventData = eventData
            }
        }
    }
    
    deinit {
        print("deinitCalendarVC")
    }
    
    @IBAction private func returnEventProfile(segue:UIStoryboardSegue) {
        configure()
        calendarView.contentController.refreshPresentedMonth()
    }
}

extension CalendarViewController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate, CVCalendarViewAppearanceDelegate {
    func presentationMode() -> CalendarMode{
        return .monthView
    }
    func firstWeekday() -> Weekday{
        return .monday
    }
    
    func didSelectDayView(_ dayView: DayView, animationDidFinish: Bool) {
        
        if let event = RealmManager.getEvent(forDate: dayView.date.convertedDate()!) {
            AlertMessage.workingOldEvent(titleMessage: nil, alertMsg: nil, { [weak self] (action) in
                
                switch action {
                case .Delete:
                    if event.isFormed {
                        AlertMessage.simpleAlertMessage(titleMessage: "Warning", alertMsg: "You cann`t delete this event because it is formed")
                    } else {
                        AlertMessage.simpleWithActionOkAlertMessage(titleMessage: "Do you want to delete this event and all team`s payments?", alertMsg: nil) {
                            RealmManager.deleteAllPaymentsForEvent(event: event)
                            RealmManager.delete(realmObject: .Event, identity: event.identity)
                            self?.configure()
                            self?.calendarView.contentController.refreshPresentedMonth()
                        }
                    }
                case .Edit:
                    if event.isFormed {
                        AlertMessage.simpleAlertMessage(titleMessage: "Warning", alertMsg: "You cann`t edit this event because it is formed")
                    } else{
                        self?.eventData.identity = event.identity
                        self?.performSegue(withIdentifier: segueGoToEventDetails, sender: self)
                    }
                case .GoToEvent:
                    let viewController = UIStoryboard(name: "Event", bundle: nil).instantiateViewController(withIdentifier: "Event") as! EventViewController
                    viewController.editEventIdentity = event.identity
                    self?.navigationController?.pushViewController(viewController, animated: true)
                }
            })
            
        } else {
            AlertMessage.creatingNewEvent(titleMessage: nil, alertMsg: nil) { [weak self] (type) in
                self?.eventData.date = dayView.date.convertedDate()!
                self?.eventData.isTraining = type == .Training ? true : false
                self?.performSegue(withIdentifier: segueGoToEventDetails, sender: self)
            }
        }
    }
    
    func dotMarker(colorOnDayView dayView: DayView) -> [UIColor]{
        
        let date = dayView.date.convertedDate()
        for event in allEvents {
            if date == event.matchDate {
                if event.isTraining {
                    return [UIColor.green]
                }
            }
        }
        return [UIColor.black]
    }
    
    func dotMarker(sizeOnDayView dayView: DayView) -> CGFloat {
        return 15
    }
    
    func dotMarker(shouldShowOnDayView dayView: DayView) -> Bool {
        
        let date = dayView.date.convertedDate()
        for event in allEvents {
            if date == event.matchDate {
                return true
            }
        }
        return false
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        monthLabelItem.title = date.globalDescription
    }
    
    func shouldAutoSelectDayOnMonthChange() -> Bool {
        return false
    }
}

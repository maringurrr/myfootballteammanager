//
//  AlertMessage.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 9/22/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit


class AlertMessage {

    
    internal static var alertMessageController: UIAlertController!
    
    internal static func simpleAlertMessage(titleMessage: String?, alertMsg: String?) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message: alertMsg, preferredStyle: .alert)
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        presentAlertController()
    }
    
    internal static func simpleWithActionOkAlertMessage(titleMessage: String?, alertMsg: String?, _ clousure: @escaping () -> ()) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message: alertMsg, preferredStyle: .actionSheet)
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
            _ in
            clousure()
        }))
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        presentAlertController()
    }
    
    
    internal static func changeName(titleMessage: String?, alertMsg: String?, arrayWithNames: [String], _ clousure: @escaping (_ name: String) -> ()) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message: alertMsg, preferredStyle: .actionSheet)
        
        for team in arrayWithNames {
            AlertMessage.alertMessageController.addAction(UIAlertAction(title: team, style: .default, handler: {
                _ in
                clousure(team)
            }))
            
        }
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        presentAlertController()
    }

    
    
    
    
    
    
    
    internal static func creatingNewEvent(titleMessage: String?, alertMsg: String?, _ clousure: @escaping (_ type: TypeEvent) -> ()) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message: alertMsg, preferredStyle: .actionSheet)
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Add Training", style: .default, handler: {
            _ in
            clousure(.Training)
        }))
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Add Game", style: .default, handler: {
            _ in
            clousure(.Game)
        }))
        
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        presentAlertController()
    }
    
    
    
    internal static func workingOldEvent(titleMessage: String?, alertMsg: String?, _ clousure: @escaping (_ type: ActionWithEvent) -> ()) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message: alertMsg, preferredStyle: .actionSheet)
        
        
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Delete event", style: .default, handler: {
            _ in
            clousure(.Delete)
        }))
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Edit event", style: .default, handler: {
            _ in
            clousure(.Edit)
        }))
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Go to event", style: .default, handler: {
            _ in
            clousure(.GoToEvent)
        }))
        
        
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        presentAlertController()
    }

    
    
    private static func presentAlertController() {
        if let controller = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            controller.present(AlertMessage.alertMessageController, animated: true, completion: nil)
        }
        else{
            UIApplication.shared.delegate?.window??.rootViewController?.present(AlertMessage.alertMessageController, animated: true, completion: nil)
        }
        return
    }
    
}

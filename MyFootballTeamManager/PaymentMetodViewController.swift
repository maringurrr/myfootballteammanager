//
//  PaymentMetodViewController.swift
//  MyFootballTeamManager
//
//  Created by mac-001 on 10/20/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

private let rowHeight: CGFloat = 50
private let segueEditPaymentMetodsProfileVC = "editPaymentMetodsProfileVC"
private let segueCreatePaymentMetodsProfileVC = "createPaymentMetodsProfileVC"


class PaymentMetodViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var paymentMetods: [PaymentMetodCellViewModel] = []
    
    
    private func setup() {
        tableView.register(UINib(nibName: "PaymentMetodViewCell", bundle: nil), forCellReuseIdentifier: "PaymentMetodViewCell")
    }
    
    fileprivate func configure() {
        
        paymentMetods = RealmManager.allPaymentMetods().map(PaymentMetodCellViewModel.init(paymentMetod: ))
        
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configure()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueEditPaymentMetodsProfileVC {
            if let viewController = segue.destination as? PaymentMetodProfileVC {
                let indexPath = tableView.indexPathForSelectedRow!
                viewController.editPaymentMetodIdentity = paymentMetods[indexPath.row].identity
                
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    @IBAction func addNewPaymentMetodAction(_ sender: UIButton) {
        performSegue(withIdentifier: segueCreatePaymentMetodsProfileVC, sender: self)
    }
    
    @IBAction private func returnPaymentMetodProfile(segue:UIStoryboardSegue) {
        print("return")
        configure()
    }
}

extension PaymentMetodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMetodViewCell", for: indexPath) as! PaymentMetodViewCell
        
        cell.paymentMetonLabel.text = paymentMetods[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMetods.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueEditPaymentMetodsProfileVC, sender: self)
    }
}
